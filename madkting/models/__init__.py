# -*- coding: utf-8 -*-

from . import base
from . import sale_order
from . import res_partner
from . import product_template
from . import account_tax
from . import uom_uom
from . import product
from . import listeners
from . import madkting_config
from . import webhook_records
from . import mapping_products
from . import mapping_fields
from . import mapping_states
from . import stock_warehouse
